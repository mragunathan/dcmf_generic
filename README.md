# dCMF 

dCMF is a software package to perform [Deep Collective Matrix Factorization for Augmented Multi-View Learning](https://arxiv.org/abs/1811.11427). 

The sample data used in the documentation examples can be downloaded from [here](https://drive.google.com/file/d/1FyTCNh5IBl6QcnO8y3X1Qide-iuLhNAK/view?usp=sharing).

## dCMF prerequisite 
- Python 3.6 
- PyTorch 0.4.0
- NumPy 1.14.0
- SciPy 1.0.0
- scikit-learn 0.19.1
- GPy 1.9.6 (modified)
	- Steps to setup  
		`git clone https://github.com/mragunathan/GPy.git`  
		`cd GPy`  
		`git checkout devel`  
		`python setup.py build_ext --inplace`  
		`nosetests GPy/testing`  
		`python setup.py install`  
- GPyOpt 1.2.5 (modified)
	- Steps to setup  
		`git clone https://github.com/mragunathan/GPyOpt.git`  
		`cd GPyOpt`  
		`python setup.py develop`  


## Running dCMF 

Following are the 2 ways to run dCMF

- **dcmf** -  To run dCMF for arbitrary collection of matrices and given hyperparameter setting. See `doc/dcmf_documentation_and_example_usage.ipy` for documentation and example.

- **dcmf_bo** - To run dCMF for arbitrary collection of matrices and perform best hyperparameter search using Bayesian Optmization(BO). The best hyperparamter setting can either be selected based on (lowest) training loss or (highest) validation performance. See `doc/dcmf_bo_documentation_and_example_usage.ipy` for documentation and example.


